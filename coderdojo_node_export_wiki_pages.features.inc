<?php
/**
 * @file
 * coderdojo_node_export_wiki_pages.features.inc
 */

/**
 * Implements hook_node_info().
 */
function coderdojo_node_export_wiki_pages_node_info() {
  $items = array(
    'wiki' => array(
      'name' => t('Wiki page'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
